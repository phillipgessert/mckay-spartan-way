% The Spartan Way
% Brett and Kate McKay

© 2019 Brett and Kate McKay

| Cover Design by Derek Hart and Book Design by Phillip Gessert
| ISBN 978-0-9993222-2-2

<div id="toc"><ul></ul></div>

<div id="body-matter">

# <span class="chapter">Introduction</span>

To some, the Spartans represent the ultimate warriors---fierce, fearless, liberty-loving, physically-ripped superhero-esque figures. The epitome of rough and ready virility.

To others, the Spartans are a repugnant people---brutish, cruel, one-dimensional proto-totalitarians. Holders of slaves, exacters of infanticide, practitioners of pederasty.

Neither view captures the complexities---not to say conflicting accounts---of the city-state known anciently as Lacedaemon.

Courageous warriors? Surely the Spartan reputation for martial prowess was well-earned. But the Spartan warrior did not fight in the way we most often idealize---in single combat, for individual glory---but rather subsumed as one cooperative member of a larger phalanx.

Nor was the Spartan man a one-trick pony, possessed solely of martial skill and knowledge. Rather, he was an aristocratic gentleman, schooled not only in war, but in music, singing, dance, rhetoric, logic, philosophy, and disciplined comportment as well. He was a literate lover of both sports and poetry, physical sparring and oral repartee. As opposed to the image of a barren, artistically and intellectually austere culture, the philosopher Sphaerus asserted that “no one was more devoted to music and song,” Spartan dance and choral festivals attracted visitors from near and far, and Socrates argued that “The most ancient and fertile homes of philosophy among the Greeks are Crete and Sparta.”

When it comes to slavery, infanticide, and pederasty, the evidence is conflicting as to the exact nature and extent to which these customs were practiced. The Spartans did subjugate the Messenians but they were more like medieval serfs than slaves and enjoyed many more privileges than did those held in other parts of ancient Greece; for this reason thousands of slaves from Athens fled to Sparta for better treatment. It is said that the Spartans killed babies deemed unfit to live by exposing them or throwing them off Mt. Taygetus, but the remains of no infants have been found there, and however and wherever infanticide did take place, it was hardly unique to Sparta but practiced in Athens and other city-states as well. As to pederasty, there are certainly sources that attest to its practice, but also those---like the account of the Athenian historian Xenophon, who is the only source from that period with firsthand experience of the *agoge* (the Spartan system for training the young) and enrolled his own sons in this school---who deny it took place. Whatever the extent of those practices of this ancient culture we find abhorrent in the modern day, they can only be fully understood, if not justified, by the singular focus the Spartans placed on creating an indomitable society of warriors, and the fact that much of the polis’ culture was structured around, and subordinated to, this aim.

As to what kind of state Sparta was, even ancient observers couldn’t decide if its government could better be described as a monarchy, a democracy, or an oligarchy. As Paul Rahe observes in *The Spartan Regime*, “Lacedaemon was, in fact, all and none of the above.” Even to describe the Spartan polis (and its neighbors) as a state is to misunderstand it, for “In antiquity, there was no Greek state. The ancient Hellenic republic was, as James Madison would later observe, ‘a pure democracy…a society consisting of a small number of citizens, who assemble and administer the government in person.’ The polis really was, as the Greeks often remarked, the men.”

We should not be surprised that multiple and sometimes conflicting views of Sparta exist, as much of what is known originates in sources biased one way or the other---from either champions or enemies of the city-state---and the amount of truly "insider" information is relatively small in size; the Spartans were a highly secretive people, constricting the travel of its citizens abroad, and the visitation of foreigners at home (indeed, this secrecy is part of what made Sparta compelling in its own time, and continues to draw our interest today). As Rahe observes, “It would not be hyperbole to appropriate for Sparta Winston Churchill’s famous description of Russia: Lacedaemon was in antiquity and remains today a riddle wrapped in a mystery inside an enigma.”

What we can know for certain is that Spartans lived a truly unique way of life. As Rahe puts it: “Classical Lacedaemon was no ordinary polis. No one thought so in antiquity; no one should think so today.”

We know too that many contemporaries of the city-state, as well as plenty of eminent observers in the centuries since, were ardent admirers of this distinctive way of life.

The philosopher Plato said that the culture of Lacedaemon had a tendency to give one an inferiority complex: “To look at the temperance and orderliness, the facility and placidity, the magnanimity and discipline, the courage and endurance, and the toil-loving, success-loving, honor-loving spirit of the Spartans, you would count yourself but a child.”

Plato was hardly the only ancient to admire Sparta from afar. Foreign visitors, including teachers like Libanius and statesmen like Cicero, came from all corners of civilization to see for themselves the legendary agoge and even, like Xenophon, enrolled their own sons in the program and made significant financial donations to it.

For centuries after its decline, Sparta continued to be venerated as a polis uncorrupted by luxury and commerce, as a model of the virtues of simplicity, precision, self-sacrifice, martial vigor, mental fortitude, and physical stamina, and as an inspiration for a balanced, mixed government. In drawing up the American constitution, the Founding Fathers found inspiration in what Thomas Jefferson called “the rule of military monks,” while Samuel Adams hoped the new republic would become a “Christian Sparta.”

If these “Laconophiles” overly idealized the Spartan city-state, it’s still worth considering what it was that drew their praise. If the details of the Spartan way of life are sometimes in dispute, or embellished, they still point to underlying principles---values and lessons we can’t and wouldn’t want to exactly replicate today, but which nonetheless impart insights on how to better live our lives. As Rahe observes:

> “we may prefer the Athenians, regarding them as more like ourselves, and we may well be right not only in that judgment but in our moral and political preferences as well. Our predilections notwithstanding, however, we name sports teams after the Spartans, and it is about them (and not the Athenians) that we ordinarily write novels and make films—which says a great deal about the ancient Lacedaemonians and perhaps also something about the unsatisfied longings that lurk just below the surface within modern bourgeois societies.”

A city the Roman historian Livy called “memorable not for the magnificence of its buildings, but for its discipline”; protected by what the mythical founder of its military described as a “wall of men, instead of bricks”; populated by those who considered themselves the descendants of “Heracles the unconquered”---a tiny warrior community that managed to command the respect of its neighbors and leave a legend for all time---undoubtedly has much to teach about the nature of these longings, and how they might be fulfilled, at least slightly, in the present age.

In three subsequent chapters, we’ll look at what lessons the ancient Spartans can offer modern men.

# Chapter 1: <span class="chapter">Manhood Is a Journey</span>

In all cultures and times, a male was not automatically considered a man, but rather had to *earn* that title by passing through rites of passage and their attendant challenges and tests. Manhood was gained through a step-by-step process, in which a boy progressively gained more knowledge and responsibilities, as well as privileges.

Sparta institutionalized this process in the form of the famous *agoge.* Meaning “a leading,” this thirteen-year physical, military, religious, and moral education trained a boy in the aptitudes, knowledge, and virtues he would need to become a man and join the *Homoioi*---the “Equals” or “Peers.” Only these Spartiates could become full citizens of Lacedaemon and join its elite warrior class. Only these could be considered true Spartan men.

Rites of passage all around the world are structured in three phases: the initiate is first separated from his former life in preparation for creating a new identity; he then exists in a liminal, in-between state in which he is no longer part of his old life, but not yet fully inducted into his new one, and is taught the knowledge necessary for one day entering that future state; finally, having studied, practiced, passed the necessary tests, and proven himself worthy, the graduate is re-introduced into his community, which recognizes and honors his new status within the group. The Spartan agoge included these stages both on a macro and micro level; the entirety of the thirteen-year training course was one long rite of passage that transitioned a young Spartan male from childhood to puberty to youth, and finally into manhood, while smaller passages progressed him from one age to the next.

When a Spartan boy was seven, he left his family to be schooled with boys his own age in the agoge. His novice status was marked by his shaved head, single simple cloak, and the fact that the only weapon he could bear was that of the sickle---a tool of the helots (the servile class). These new apprentices were not only instructed in physical education, but also taught reading, writing, dancing, and singing, as well as logic, rhetoric, and philosophy.

When the Spartan boy became an adolescent, his growing status was symbolized in the fact he could now grow his hair out short. His instruction in arts and academics continued, while his physical training became more intense, including participation in team sports, which enjoyed far more prevalence in Sparta than other areas of ancient Greece. Ball games and other athletics were played even in the hottest heat of the summer, in order to further the boys’ capacity for toughness and endurance.

Adolescents were broken into different age classes, which were overseen by older teachers, as well as by a peer leader the same age, and a 20-year-old recent graduate of the agoge. To move from one age class to the next, the young trainee had to pass certain tests and competitions concerning strength, courage, aptitude, endurance, and even refinement.

For young males were not only taught the conduct of men in terms of physical and martial skills, but in manners as well. As Nigel M. Kennell notes in *Gymnasium of Virtue*, the adolescent was introduced “to the ways of a young Spartan gentleman: to keep his hands inside his robe while in public, to walk without talking, to keep his eyes always on the ground, and never to stare.” During this phase of maturation, a young trainee might be brought in front of a table of banqueting grown men and questioned, in a sort of catechism-like style, on the values and philosophy of the Spartan way of life.

As Kennell explains, further accountability for behavior was found in the fact that veteran members of the Lacedaemonian community paid close attention to the development of its youth:

> “Plutarch emphasizes that the old men of Sparta kept watch over the young, attending their workouts in the gymnasium and their games and taking note of their general comportment throughout the day. Simply by their presence, they inspired fear in those likely to transgress and reinforced the shame and the yearning for excellence which guide those inclined to be virtuous.”

Around the ages of 18--20---the threshold of manhood---the training of the increasingly seasoned apprentice intensified and he took part in more rigorous physical exercise, hunting outings, competitions in sports and gymnastics, and mock battles using the real weapons and equipment of the Spartan warrior.

The separation phase of the agoge’s extended rite of passage intensified as well in an experience called the *krupteia* or *krypteia*---a name which derives from the word for “secret” or “hidden.” For one year, the Spartan youth had to seclude himself from the polis, living off the land in the countryside, without being seen by the general population. Unarmed and without servants, shoes, or bedding, the experience was designed to test the youth in stealth, resourcefulness, and self-reliance; it was described by Plato as a “wonderfully severe training in hardihood.” (It should be noted that the krypteia is thought by some to be the name for a select, secret force of Spartans who spied on and policed the helot population by night, or for a kind of special operations wing of the Spartan military. Other scholars however, including Kennell, who conducted one of the most extensive studies of the agoge, argue that the term krypteia only rightly applies to this year-long course in bushcraft and wilderness survival, in which *all* young Spartan males participated.) A test not only of skill and guerrilla-esque adeptness, but also of the ability to thrive in solitude, the krypteia was a culmination of the lessons in courage, toughness, and discipline a young man had been mastering throughout the agoge---another transition point to crown a thirteen-year series of them.

If the trainee successfully completed this challenge, and those which had come before, at age 20 he graduated from the agoge to become a full-time soldier. At this point, he was to join one of the men’s dining clubs (more on these later), in which his mentoring into manhood continued, as he nightly ate with men of different ages, with many years of wisdom and experience between them. As Rahe notes, these dining clubs “integrat\[ed\] him into the larger community by means of an all-male social unit of a size perfect for engaging and keeping his loyalties.”

From ages 21--30, a Spartan man served on active duty in the military. After turning 30, he transitioned to the reserves, and was expected to get married, if he hadn’t done so already, and start a family. He could now grow his hair out in the long, Lacedaemonian style, and was considered a full citizen, one of the Equals, the Peers. He had earned his place among Spartan men.

Throughout all of these phases, tests, and challenges, failure was always a possibility---a young man could flunk the agoge, and disqualify himself from joining the ranks of the Homoioi---a humiliating disgrace.

But if he proved worthy, a Spartan male not only moved chronologically and biologically from childhood, to puberty, to youth, but transformed in skill, knowledge, and confidence as he moved from plebe to warrior to citizen. Each stage and rite of passage carried symbology that indicated his current status, while imparting the instruction and mentoring that prepared him for the next, so that, when he reached the subsequent stage in his journey into manhood, he knew what was expected of him, and had no doubt he belonged.

# Chapter 2: <span class="chapter">Brotherhood Is Born in Breaking Bread</span>

> “the Spartiates constituted a seigneurial class blessed with leisure and devoted to a common way of life centered on the fostering of certain manly virtues. They made music together, these Spartans. There was very little that they did alone. Together they sang and they danced, they worked out, they competed in sports, they boxed and wrestled, they hunted, they dined, they cracked jokes, and they took their repose. Theirs was a rough-and-tumble world, but it was not bereft of refinement and it was not characterized by an ethos of grim austerity, as some have supposed. Theirs was, in fact, a life of great privilege and pleasure enlivened by a spirit of rivalry as fierce as it was friendly. The manner in which they mixed music with gymnastic and fellowship with competition caused them to be credited with *eudaιmonía*—the happiness and success that everyone craved—and it made them the envy of Hellas.”
> 
> ---Paul Rahe, *The Spartan Regime*

No man wins any battle, of any kind, completely by himself, but rather requires a team of comrades. This truth was never so literally and viscerally brought to life than in the Spartan phalanx.

Spartan hoplites were distinguished by the heavy wooden shield they carried on their left arm; 15 pounds in weight, 3 feet in diameter, it was, Rahe notes, “an encumbrance more unwieldy and awkward than we are apt to imagine.” It was also nearly useless to a soldier fighting outside the phalanx; when facing the enemy, the shield “left the right half of \[the soldier’s\] body unprotected and exposed, and it extended beyond him to the left in a fashion of no use to him as a solo performer.” Thus “when infantrymen equipped in this fashion were operating on their own, cavalry, light-armed troops, and enemy hoplites in formation could easily make mincemeat of them.”

A hoplite’s shield was only effective when it was one of many employed in the phalanx formation. His shield protected the right half of the man to his left, while his neighbor’s shield protected the right half of *his* body. Phalanx warfare wasn’t about individual, Achilles-style glory. Each soldier critically needed the man next to him. Alone, each warrior was vulnerable and weak; together in the phalanx, their shields formed an interlocking wall of men that could defend against the blows of the enemy and push forward in strength.

Given this structure of mutual dependence, a phalanx was only as strong as its weakest link, and each man had to absolutely trust the brother next to him to give his all; if everyone held together, casualties were greatly minimized; if one man fell apart, he exposed everyone to greater danger. Unity and loyalty were thus paramount.

What motivated the Spartan warrior to refuse to be the weak link and to stand his ground in the heat of battle?

Shame was one powerful source of motivation. Shame has become something of a dirty word in modern times, but few forces more strongly compel behavior. A fear of shame is in fact only the necessary flip side of a love for honor; in an honor group, men not only strive for excellence but flee from disgrace. The Spartans cared deeply about maintaining the respect of their peers---their fellow equals; said Isocrates, they “think nothing as capable of inspiring terror as the prospect of being reproached by their fellow citizens.”

For a Spartan warrior to let down his brothers in combat would not only invite shame from his comrades, but humiliation from his entire community. As the Spartan poet Tyreatus writes, to return home a coward was the ultimate dishonor:

> “Of those who dare to stand by one another and to march\
> Into the van where the fighting is hand to hand,\
> Rather few die, and they safeguard the host behind.\
> But for the men who are tremblers all virtue is lost.\
> No one can describe singly in words nor count the evils\
> That come to a man once he has suffered disgrace.”

While the fear of shame certainly played a powerful role in motivating a Spartan warrior to hold the line in battle, he was also driven by a still greater, and higher, force: love for his brothers-in-arms, who were also his friends and his family.

This love was developed from many of Lacedaemon’s unique institutions and traditions.

Partly it came from men growing up together in the *agoge*, and sharing both in its (oft-overlooked) pleasures and its famous hardships. Partly it came from the rule that all men under age 30 had to sleep at night in the barracks, rather than at home.

But arguably, the strongest force welding Spartan men together took the form of a tradition that lasted their whole life through: the *syssitia*---Lacedaemon’s fraternal, all-male messes. 

## The Syssitia and the Brotherhood Born From Breaking Bread

Once called *andreia*---literally “belonging to men”---the syssitia, Rahe explains, “was not just an arrangement for meals. It was an elite men’s club, a cult organization, and, at the same time, the basic unit in the Spartan army.”

At age 20, a Spartan man was required to join one of these dining clubs if he wished to become a member of the *Homoioi*. Each mess had about 15 members, and like modern fraternities, each likely had its own “character”---with certain associations to family lines, personality types, political and philosophical leanings, and so on. The recent agoge grad had to apply to the syssitia he wished to join, and its members would vote on his acceptance; the vote had to be unanimous---if one member blackballed the candidate, he was out. If you were accepted, you became a member for life.

Once a man had been accepted to a mess, he was required to dine at it each evening---not even kings could be absent without a worthy excuse.

Each member of a dining club contributed to the mess’s stores that were used to make the nightly meal, repast which consisted primarily of a “black broth” made from pork, salt, vinegar, and blood.

As Kennell explains, however, “once the prescribed rations had been consumed,” the club’s members engaged in the “typically Greek propensity for competitive displays of generosity.” Members treated their messmates to foods that had to be, as the rules of the syssitia dictated, either raised/grown on their land or hunted themselves, and which could have included wild game, olives, fruits, vegetables, herbs, nuts, eggs, milk, cheese, butter, and wheat bread. “Before serving, the cooks announced the name of the day’s donor to his grateful companions so that they might appreciate his hunting prowess and diligence for them.”

Over this simple fare (and occasional delicacies), as well as the moderate drinking of wine (drunkenness was disdained), Spartan messmates spoke freely with one another about both civic and personal affairs. While Athenian men conversed publicly about politics and philosophy in the agora, Spartan men did so in private, amongst the comrades they respected and trusted, their daily banquets providing a confidential, comfortable sanctuary in which to hold such exchanges.

Men from many different ages and stages of life belonged to a mess, and the old mentored the young; as Kennell writes, the syssitia “bec\[ame\] at Sparta the preeminent medium for reinforcement of Sparta’s aristocratic warrior ethic.”

Syssitia conversations certainly weren’t all serious, however; Spartans themselves were hardly the somber and humorless drones we often picture them as. Lacedaemon was in fact one of only two Greek city-states to build a temple to the god of laughter. And according to Heraclides, Spartan youngsters were taught “Laconic” wit (more on this form of speech later) from an early age, seemingly to simply be better able to zing each other: “Immediately from childhood on they practice speaking tersely, then good-natured bantering back and forth.”

This affection for jesting continued through a Spartan man’s whole life and constituted an important part of syssitia culture; as Karl Otfried Müller wrote, “In common life, laughter and ridicule were not unfrequent at the public tables; to be able to endure ridicule was considered the mark of a Lacedæmonian spirit.” 

Good-natured ribbing has long formed much of the unique camaraderie that exists between men, as such teasing (which includes the giving of nicknames) constitutes a paradoxical way of demonstrating the solidity of their bonds. Men will swap insults to toughen each other up, while testing and strengthening the relationship; if you can trade good-natured barbs, without the parties being offended, it indicates a significant level of trust.

At the same time, some gallows humor helped the Spartans deal with the weightier aspects of their martial vocation. As Edith Hall observes in *Introducing the Ancient Greeks*: “It is easier to practice psychological honesty about dark aspects of human existence with the protective shield of laughter…\[the Spartans\] used pointed Laconic wit to help maintain the morale of their warrior culture.”

After eating, talking, and joking, the men would raise the paean, singing together as a group, and then each singing some of the verses of Tyrtaeus in turn.

The camaraderie built through sharing nightly meals in their dining clubs ultimately proved an advantage for the Spartans in combat---helping to create the unity so crucial for success in phalanx warfare. As Thomas Arnold wrote, “The object of the common tables was to promote a social and brotherly feeling amongst those who met at them; and especially with a view to their becoming more confident in each other, so that in the day of battle they might stand more firmly together, and abide by one another to the death.”

But the syssitia’s benefit was hardly limited to war; it served as a lifelong source of friendship and support in peace as well.

Indeed, the Spartan dining clubs were also known as *pheiditia*---a term likely derived from *philitia*, or “love-feast.”

# Chapter 3: <span class="chapter">The Mindset and Tactics of a Battle&#8209;Ready Warrior</span>

At its peak, the Spartan army was the most dominant, and feared, military force in ancient Greece, and its prowess was built on the singular mentality and strategy it brought to the art of war.

In this final chapter, we’ll take an inspiring and thoroughly fascinating tour of the essential mindset and tactics that allowed these warriors to battle fiercely and come out the victor.** **

## There Is Power in Appearance

Spartan men not only had the skills and training to back up their reputation as formidable warriors, they enhanced that reputation---and their efficacy on the battlefield---by cultivating an external appearance that matched their internal prowess.

The Spartans terrorized their enemy before they even got within spears’ length of them. As they awaited the command to advance, they stood straight and steady in formation, and everything from their clothes to their equipment bespoke strength, discipline, and ferocity.

Spartan warriors were clothed in a scarlet tunic and cape (discarded prior to battle), for, Xenophon tells us, the color was thought to have “the least resemblance to women’s clothing and to be most suitable for war.” The latter statement gave rise to the apocryphal idea that red was also chosen because it hid blood better---concealing a wound, and a weakness, from the enemy.

Over his tunic and hung from his arm the Spartan hoplite carried armor and a shield which had been buffed to a brilliant shine and glinted in the sun.

Spartan men wore their hair long---a style which had once been common all over Greece, but which Lacedaemonians held onto after other city-states had shifted to shorter cuts. For the Spartans, long hair symbolized being a free man, and they believed, Plutarch says, “that it made the handsome more comely and the ugly more frightful.” The Spartans kept themselves well-groomed, often braiding these long locks, and keeping their beards neatly trimmed as well.

Atop their heads was placed a crowning piece of equipment which the narrator of Steven Pressfield’s *Gates of Fire* (a work of historical fiction accurate in many details) describes as the “most frightful of all”:

> “Adding further to the theater of terror presented by the Hellenic phalanx…were the blank, expressionless facings of the Greek helmets, with their bronze nasals thick as a man’s thumb, their flaring cheekpieces and the unholy hollows of their eye slits, covering the entire face and projecting to the enemy the sensation that he was facing not creatures of flesh like himself, but some ghastly invulnerable machine, pitiless and unquenchable.”

The formidable appearance of the Spartan helmet was further enhanced by the fact it was “overtopped with a lofty horsehair crest which as it trembled and quavered in the breeze not only created the impression of daunting height and stature but lent an aspect of dread which cannot be communicated in words but must be beheld to be understood.”

The clothing and equipment of the Spartan warrior worked to his advantage in two ways: 1) it made the soldier himself feel more ferocious, more invincible, more confident, and 2) it intimidated the living daylights out of his foe.

The power of the Spartans’ appearance softened up the enemy line before they even hit it, and added to a reputation for strength that sometimes deterred enemies from even going to battle against them at all.** **

## Always Perform a Pre-Battle Ritual

> “Keep your men busy. If there is no work, make it up, for when soldiers have time to talk, their talk turns to fear. Action, on the other hand, produces the appetite for more action.” 
> 
> ---*Gates of Fire*

In Herodotus’ *Histories*, he writes that during the lead up to the battle of Thermopylae, King Xerxes, ruler of the Persian empire, “sent a mounted scout to see how many \[Spartans\] there were and what they were doing.” What did the scout observe? “He saw some of the men exercising naked and others combing their hair.”

Before battle, Spartan warriors kept their nerves at bay by staying busy with various tasks and physical rituals. In their youth, they had memorized verses of the poet Tyrtaeus, which they recited to themselves and sang and chanted as they marched on campaign. In the days prior to battle, they exercised before breakfast, had further military instruction and training after eating, and engaged in exercise and athletic competitions in the afternoon. During moments of repose, the men dressed and groomed their hair, and polished the brass exteriors of their shields.

When the time came to march on the enemy, the playing of a flute allowed the Spartans to perfectly keep time, and as a result of this music, as well as their other tension-reducing, courage-buoying rituals, they advanced upon the enemy in a slow, steady procession, which only added to the intimidation factor just described above.

## A Warrior Can Be Both Fierce and Reverent

We’re apt to think of the Spartans as ferocious, cocksure warriors. But while no fighting force could be more easily excused for relying entirely on their own strength and abilities, the Spartans were in fact acutely cognizant of, and humbled by, the existence of forces greater than themselves.

The Spartans were an extremely reverent people. “From an early age,” Rahe writes, they were “imbued with a fear of the gods so powerful that it distinguished them from their fellow Greeks.” Indeed, piety served as “the foundation of Spartan morale.”

Before embarking on a campaign, every morning while on it, and immediately preceding battle, oracles were consulted, sacrifices were made, and omens were examined. The sanction, or censure, of the gods was sought for every decision.

So too, religious obligation came even before martial duty. The Spartans delayed sending a deployment to the Battle of Marathon because the call came in the middle of a religious festival. For the same reason, Leonidas sent only a small advance guard to Thermopylae instead of Lacedaemon’s main force.

The reverence of the Spartans could be called superstition, but it could also be called humility---an awareness of, and respect for, the forces of fate that ultimately, no matter one’s skill and preparation, can influence the outcome of an endeavor and cannot be wholly controlled.

## Endurance Is the Foundation of Strength

In phalanx warfare, agility, cleverness, and speed were not as important as grit, fortitude, and stamina---sheer *endurance*. The lines of hoplite soldiers pressed forward with their shields, seeking to push back the enemy line, breach its ranks, and trigger a retreat. The virtues most needed by a Spartan warrior then were commitment, discipline, and the fortitude required to stand one’s ground and grind it out. Courage was certainly needed, but not the courage of intrepid boldness, but that which modern general George S. Patton called “fear holding on a minute longer.”

Once this is grasped, one can begin to better understand the rationale behind the agoge’s famous hardships: meager rations, limited bathing, a single cloak to wear year-round in all temperatures, beds made of reeds. And of course the endless rounds of vigorous exercise and sports. As Plato noted, Spartan training really amounted to a relentless series of endurance tests.

The end sought in such training was not hardship for hardship’s sake, but an adaptability, a tolerance for pain and for changing, challenging conditions---a mental toughness that bolstered physical toughness, and vice versa. The aim was to inculcate the kind of strength most needed by a Spartan warrior: that of being able to hold the line under pressure. As Patton put it: “A pint of sweat saves a gallon of blood.”

## Speak (and Think) Laconically

The Spartan philosopher Chilon---one of the Seven Sages of Greece---famously said that “less is more,” and this was a maxim that guided the whole ethos of Lacedaemon---from its buildings to its citizens’ clothing and diet. Indeed, “Spartan” today remains a descriptor synonymous with simplicity, austerity, and frugality---a comfort with discomfort and a disdain for luxury.

The “less is more” principle also governed the language of the Spartans, who took a minimalist approach to speech which today we still refer to as “Laconic.” The ideal was to speak only when one had something important to say, and then only in short, terse bursts, pithy sayings, and the sharp, clever replies that characterized Laconic wit. The Spartans honed their words until they were as sharp as their spears---and just as sure to find their mark.

For example, legend has it that when Philip II sent a message saying, “If I enter Laconia, I will raze Sparta,” the Spartans sent but a one-word reply: “*If*.” And of course there is the famous story of the soldier at Thermopylae who lamented to Leonidas that the Persians shot so many arrows that they darkened the sun. The warrior king’s reply? “*Then we will fight in the shade*.”

Socrates thought that the Spartans’ singular style of speech was a way of strategically getting others to underestimate them:

> “they conceal their wisdom, and pretend to be blockheads, so that they may seem to be superior only because of their prowess in battle…This is how you may know that I am speaking the truth and that the Spartans are the best educated in philosophy and speaking: if you talk to any ordinary Spartan, he seems to be stupid, but eventually, like an expert marksman, he shoots in some brief remark that proves you to be only a child.”

It was also a field expedient way of speaking---you want to get straight to the point when yelling commands in the chaos of combat.

But the Laconic tactic of conserving speech may have also been a deliberate philosophical choice; as historian Karl Otfried Müller speculated, “A habit of mind which might fit its possessor for such a mode of speaking, would best be generated by long and unbroken *silence.*” That is, if one wishes to make what he says count, he is forced to be more reflective before opening his mouth.

## Achieve Mastery in Your Domain

> “these men neither tilled the soil nor toiled at the crafts—but freed from labor and sleek with the palaestra’s oil, they exercised their bodies for beauty’s sake and passed their time in the polis…they were ready to do all and suffer all for this one accomplishment---noble and dear to human kind---that they might prevail over all against whom they marched.” 
> 
> ---Josephus

The Spartans were more multi-dimensional than often imagined: the polis was almost universally literate, excelled in music and dance, produced sculptors, philosophers, and poets, and of course engaged in an array of sports and athletics.

Nonetheless, they did undoubtedly give intense, relentless focus to one area above all others: the development of martial skill and virtue. This was the highest form of excellence---the domain in which every warrior strove to achieve absolute mastery.

The Spartans did not dabble in warfare; it was the pursuit around which all culture---education, relationships, politics---was structured and disciplined. Citizens were barred from farming or practicing a trade, and even from possessing gold or silver coins; without the distractions of commerce and material acquisition, they could concentrate wholly on mastering the way of the warrior. Rahe writes:

> “The Spartans were, as Plutarch remarks, ‘the servants of Ares,’ not Mammon. They were ‘the craftsmen of war,’ not the makers of pots. They had but one purpose in life: to gain a reputation for valor.”

While the militiamen of other cities spent the months outside the fighting season as farmers or craftsmen or merchants, the Spartans were full-time soldiers. As Plutarch observed, “they were the only men in the world for whom war brought a respite in the training for war.”

Dedicating themselves wholly to their vocation, they became the best at what they did, with an advantage over those who were mere dilettantes in the martial arts; in an episode recounted by Plutarch, the Spartan king Agesilaus sought to convince Lacedaemon’s allies to join the polis in a war against Thebes, by essentially arguing that a single Spartan warrior was worth more than several men from other city-states:

> “The allies said they had no wish to be dragged this way and that to destruction every year, they themselves so many, and the Lacedaemonians, whom they followed, so few. It was at this time, we are told, that Agesilaus, wishing to refute their argument from numbers, devised the following scheme. He ordered all the allies to sit down by themselves, and the Lacedaemonians apart by themselves. Then his herald called upon the potters to stand up first, and after them the smiths, next, the carpenters, and the builders, and so on through all the handicrafts. In response, almost all the allies rose up, but not a man of the Lacedaemonians; for they were forbidden to learn or practice a manual art. Then Agesilaus said with a laugh: ‘You see, men, how many more soldiers than you we are sending out.’”

## Fight From Habit, Not Feeling

As a result of this extraordinary focus on mastering a single domain---thirteen years of dedicated training, ten years of practice and real-life execution as a full-time soldier, and decades more of martial maintenance in the reserves---the ways of war become ingrained in the sinews of a Spartan soldier. Pressfield compares the preparation of this force with that of the militiamen mustered by other city-states:

> “This process of arming for battle, which the citizen-soldiers of other poleis had practiced no more than a dozen times a year in the spring and summer training, the Spartans had rehearsed and re-rehearsed, two hundred, four hundred, six hundred times each campaigning season. Men in their fifties had done this ten thousand times. It was as second-nature to them.”

The summer soldier was not accustomed to the sights, sounds, and hardships of war; their hands had not been calloused around the shaft of a spear; their backs had not gotten used to the weight of their armor; their eyes had not become inured to the sight of an advancing foe. Courage in these unfamiliar circumstances was a matter of trying to gin up a feeling---an emotion rallied in the supportive, rah-rah safety of one’s own line, and then utterly vaporized by contact with the enemy’s.   

For the Spartans, courage was not a vulnerable and transitory state of mind, but the product of preparation and practice. In fact, they did not respect the solider who fought in an impassioned rage, believing such loud and belligerent posturing was used to hide one’s fear and lack of self-composure. Instead, they sought to embody the ethos of “the quiet professional” who simply sets out to do his job, and lives the classic motto voiced by coaches like Vince Lombardi: “*Act like you’ve been there before*.”

The courage of the Spartans was not born of feeling, but discipline.

It was not an emotion, but a habit.

Or as Pressfield observes in *Gates of Fire*, “War is work, not mystery.”

## Conquer or Die

> “And he who falls in the front ranks and gives up his spirit\
> So bringing glory to the town, the host, and his father\
> With many a wound in his chest where the spear from in front\
> Has been thrust through the bossy shield and breastplate\
> This man they will lament with a grievous sense of loss.”
>
> “And disgraceful is the corpse laid out in the dust,\
> Thrust through from behind by the point of a spear.”
>
> ---Tyrtaeus

After the Battle of Thermopylae, a monument was placed atop the burial mound, where the last of the 300 Spartans died defending the pass, which reads:

> “Go tell the Spartans, stranger passing by, that here obedient to their laws we lie.”

The epigraph is famous, but what was the “law” exactly to which these warriors stayed true?

According to Herodotus, the exiled Spartan king Demaratus gave an answer to Xerxes on the eve of the battle, when the Persian “King of Kings” inquired as to how much resistance to expect from the Greeks:

> “As for the Spartans, fighting each alone, they are as good as any, but fighting as a unit, they are the best of all men. They are free, but not completely free—for the law is placed over them as a master, and they fear that law far more than your subjects fear you. And they do whatever it orders—and it orders the same thing always: never to flee in battle, however many the enemy may be, but to remain in the ranks and to conquer or die.”

The Spartan heading into battle didn’t save anything for the way back; he faced the enemy head on without thought of retreat. He lived the ethos embodied in the charge given him by his mother and wife as he left for battle: “Come back with your shield or on it.”

This, ultimately, was the Spartan way.

*With it or on it*.

# Selected Bibliography

<div class="bibliography">

*History of Rome* by Thomas Arnold

*Introducing the Ancient Greeks: From Bronze Age Seafarers to Navigators of the Western Mind* by Edith Hall

*Histories* by Herodotus

*The Gymnasium of Virtue: Education and Culture in Ancient Sparta* by Nigel Kennell

*The History and Antiquities of the Doric Race* by Karl Otfried Müller

*Gates of Fire* by Steven Pressfield

*The Spartan Regime: Its Character, Origins, and Grand Strategy* by Paul Rahe

</div>